# Appssemblea

Aplicació per a la realització d'assemblees presencials mitjançant un dispositiu mòbil per agilitzar el procés de recompte de vots. 

## Informació general

Aquesta aplicació té l'objectiu d'agilitzar el procés de votació i recompte de vots d'una assemblea a través de l'utilització del dispositiu mòbil.

Entre les seves característiques principals podem trobar:

* Crear espais de votació
* Modificar espais de votació
* Realitzar la votació
* Consultar els resultats de les votació
* Comprovar l'accés a l'espai de l'assemblea
* Consultar la documentació de l'assemblea

## Imatges de aplicació

![Perfil usuari](perfils/imatges/membre/perfil_membre.png){: style="width:50%"}


![Consulta resultats](perfils/imatges/compromissari/resultats_compromissari.png){: style="width:50%"}
