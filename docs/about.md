# Sobre Appssemblea

Appssemblea és un projecte de final de màster creat per Joel Monné i Mesalles.

## Autor

Joel Monné i Mesalles.
[Perfil GitLab.com](https://gitlab.com/joetds)

## Llicència

Aquesta obra està subjecta a una llicència de
[Reconeixement-NoComercial-CompartirIgual 3.0 Espanya de Creative Commons](http://creativecommons.org/licenses/by-nc-sa/3.0/es/)