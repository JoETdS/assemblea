# Tipus d'usuari

Aquest manual d'usuari està dividit entre els 3 tipus de perfils d'usuari existents. Ordenats de més a menys poders dins l'aplicació.

## Tècnic

Opcions de l'usuari:

* [`Accés a l'espai de votació`](../perfils/tecnic/#acces)
    * Controlar l'accés a l'espai de votació.
* [`Espais de votació`](../perfils/tecnic/#espai-de-votacio)
    * Crear espais de votació.
    * Modificar espais de votació.
    * Veure resultats de les votacions.
* [`Documentació`](../perfils/tecnic/#documentacio)
    * Pujar documentació pels diferents espais de votació.
    * Editar documentació pels diferents espais de votació.

## Membre compromissari

Opcions de l'usuari:

* [`Accés a l'espai de votació`](../perfils/compromissari/#acces)
    * Poder accedit a l'espai de votació.
* [`Espais de votació`](../perfils/compromissari/#espai-de-votacio)
    * Poder votar.
    * Veure resultats de les votacions.
* [`Documentació`](../perfils/compromissari/#documentacio)
    * Veure documentació dels epsais de votacions.

## Membre associatiu

Opcions de l'usuari:

* [`Accés a l'espai de votació`](../perfils/membre/#acces)
    * Poder accedir a l'espai de votació.
* [`Espais de votació`](../perfils/membre/#espai-de-votacio)
    * Veure resultats de les votacions.
* [`Documentació`](../perfils/membre/#documentacio)
    * Consultar la documentació d'un espai de votació.