# Compromissari

Perfil amb els dret a votar durant l'assemblea.

## Característiques

Opcions de l'usuari:

* [`Accés a l'espai de votació`](#acces)
    * Accedir a l'espai de votació.
* [`Espais de votació`](#espai-de-votacio)
    * Realitzar votació.
    * Veure resultats de les votacions.
* [`Documentació` ](#documentacio)
    * Consultar la documentació d'un espai de votació.

## Accés

    Poder accedir a l'espai de votacions
    
L'usuari podrà accedir a l'espai de votació a través del seu QR personal.

![Perfil usuari](./imatges/compromissari/perfil_compromissari.png){: style="width:50%"}
    
## Espai de votació

    Realització votació
   
L'usuari podrà executar el seu vot en una votació

![Realització votació](./imatges/compromissari/vot_compromissari.png){: style="width:50%"}


    Veure resultats de les votacions
   
L'usuari podrà consultar els resultats de les votacions.

![Consulta resultats](./imatges/compromissari/resultats_compromissari.png){: style="width:50%"}

    
## Documentació

    Consultar la documentació d'un espai de votació

L'usuari podrà consultar la documentació que pertanyi a una votació.

![Consulta documentació](./imatges/compromissari/documentacio_compromissari.png){: style="width:50%"}
