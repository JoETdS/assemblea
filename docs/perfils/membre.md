# Membre associatiu

Perfil amb els dret a assitir a l'assemblea.

## Característiques

Opcions de l'usuari:

* [`Accés a l'espai de votació`](#acces)
    * Accedir a l'espai de votació.
* [`Espais de votació`](#espai-de-votacio)
    * Veure resultats de les votacions.
* [`Documentació` ](#documentacio)
    * Consultar la documentació d'un espai de votació.

## Accés

    Poder accedir a l'espai de votacions
    
L'usuari podrà accedir a l'espai de votació a través del seu QR personal.

![Perfil usuari](./imatges/membre/perfil_membre.png){: style="width:50%"}
    
## Espai de votació

    Veure resultats de les votacions
   
L'usuari podrà consultar els resultats de les votacions. 

![Consulta resultats](./imatges/membre/resultats_membre.png){: style="width:50%"}

    
## Documentació

    Consultar la documentació d'un espai de votació

L'usuari podrà consultar la documentació que pertanyi a una votació.

![Consulta documentació](./imatges/membre/documentacio_membre.png){: style="width:50%"}
