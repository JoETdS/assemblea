# Tècnic

Perfil amb els poders per l'organització de l'assemblea.

## Característiques

Opcions de l'usuari:

* [`Accés a l'espai de votació`](#acces)
    * Controlar l'accés a l'espai de votació.
* [`Espais de votació`](#espai-de-votacio)
    * Crear espais de votació.
    * Modificar espais de votació.
    * Veure resultats de les votacions.
* [`Documentació` ](#documentacio)
    * Pujar documentació pels diferents espais de votació.
    * Editar documentació pels diferents espais de votació.

## Accés

    Permetre l'accés a l'espai de votacions
    
L'usuari tècnic podrà controlar l'accés de les persones a l'espai de votació a través dels QRs dels usuaris.

![Accés lectura QR](./imatges/tecnic/tecnic_usuari.png){: style="width:50%"}
    
## Espai de votació

    Crear/Editar votacions
   
L'usuari tècnic podrà crear noves votacions en l'espai de votació.

![Accés espai creació](./imatges/tecnic/crear_ambit.png){: style="width:50%"}

### Crear o Editar:

    Crear noves votacions
   
L'usuari tècnic podrà crear noves votacions en l'espai de votació.

![Accés espai creació](./imatges/tecnic/llistat_ambits_edit.png){: style="width:50%"}
    
    Editar votacions existents
   
L'usuari tècnic podrà editar votacions en l'espai de votació.

![Accés espai creació](./imatges/tecnic/un_ambit_edit.png){: style="width:50%"}
    
## Documentació

    Pujar documentació d'un element a votar

L'usuari tècnic podrà pujar la documentació que pertanyi a una votació.

![Accés espai creació](./imatges/tecnic/pujar_documentacio_edit.png){: style="width:50%"}

Seguidament haurà d'escollir el document del seu dispositiu.

    Modificar fitxer documentació d'un elements a votar
    
L'usuari tècnic podrà canviar el fitxer de la documentació que pertanyi a una votació.

![Accés espai creació](./imatges/tecnic/editar_documentacio_edit.png){: style="width:50%"}