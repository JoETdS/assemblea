package joel.tfm.assemblea.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import joel.tfm.assemblea.MainActivity;
import joel.tfm.assemblea.R;
import joel.tfm.assemblea.adapters.FirebaseDatabaseAdapter;
import joel.tfm.assemblea.classes.User;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;

    private FirebaseDatabaseAdapter dbAdapter;
    private FirebaseAuth mAuth;
    private MaterialCardView firstLoginView;

    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        firstLoginView = findViewById(R.id.first_login_user_card);

        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Instances of databases
        this.mAuth = FirebaseAuth.getInstance();
        this.dbAdapter = new FirebaseDatabaseAdapter();

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                //finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                String email = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                loginViewModel.login(email, password);
                int hash = 31 * password.hashCode();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("password", hash);
                editor.apply();
                FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(
                    new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                boolean log = isFirstLogin(firebaseAuth.getUid());
                                if (log) {
                                    firstLogin();
                                } else {
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                    finish();
                                }
                            } else {
                                Snackbar
                                    .make(v, task.getException().getLocalizedMessage(), Snackbar.LENGTH_LONG)
                                    .show();
                            }
                        }

                    });
            }
        });

    }

    private void firstLogin() {
        firstLoginView.setVisibility(View.VISIBLE);

        fillAutocompleteTextViews();

        // Agafem el botó per desar les dades
        MaterialButton buttonCreateUser = firstLoginView.findViewById(R.id.Bcreate_user_data);

        // L'usuari prem click per desar
        buttonCreateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User currentUser = getNewUserData();
                createUser(currentUser);
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        });

    }

    private void fillAutocompleteTextViews() {

        String[] arrayTypes = new String[]{"Compromisari", "Membre"};
        AutoCompleteTextView autocompleteType = (AutoCompleteTextView) findViewById(R.id.TVuser_type);
        ArrayAdapter<String> autoAdapterType;
        autoAdapterType = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line,
                arrayTypes);
        autocompleteType.setAdapter(autoAdapterType);

        String[] arrayGroups = new String[]{"A.E.Turribus", "A.E.Lo Mòtit", "A.E.Taciplà"};
        AutoCompleteTextView autocompleteGroup = (AutoCompleteTextView) findViewById(R.id.TVuser_agrupament);
        ArrayAdapter<String> autoAdapterGroup;
        autoAdapterGroup = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line,
                arrayGroups);
        autocompleteGroup.setAdapter(autoAdapterGroup);
    }

    private User getNewUserData() {
        TextInputEditText name = firstLoginView.findViewById(R.id.TVuser_name);
        TextInputEditText surname = firstLoginView.findViewById(R.id.TVuser_surname);
        MaterialAutoCompleteTextView group = firstLoginView.findViewById(R.id.TVuser_agrupament);
        MaterialAutoCompleteTextView type = firstLoginView.findViewById(R.id.TVuser_type);

        return new User(mAuth.getUid(), name.getText().toString(),
                surname.getText().toString(),
                group.getText().toString(),
                type.getText().toString());
    }


    private boolean isFirstLogin(String userId) {
        return !(this.dbAdapter.userExist(userId));
    }

    private void createUser(User user) {
        this.dbAdapter.createUserData(user);
    }

    private void updateUiWithUser(LoggedInUserView model) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        //String welcome = getString(R.string.welcome) + user.getDisplayName();
        // TODO : initiate successful logged in experience
        //Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}