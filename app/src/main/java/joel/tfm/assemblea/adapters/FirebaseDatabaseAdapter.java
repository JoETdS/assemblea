package joel.tfm.assemblea.adapters;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import joel.tfm.assemblea.classes.Ambit;
import joel.tfm.assemblea.classes.User;
import joel.tfm.assemblea.classes.Vote;
import joel.tfm.assemblea.interfaces.ListenerUserFirebaseAdapter;

public class FirebaseDatabaseAdapter {

    private final String url = "https://assemblea-c8e01-default-rtdb.europe-west1.firebasedatabase.app/";

    private final String ambitPath = "ambit";
    private final String userPath = "users";
    private final String votePath = "votes";
    private final String documentPath = "documents";
    private final String imagesPath = "userImages";

    private FirebaseDatabase database;
    private FirebaseStorage storage;

    public FirebaseDatabaseAdapter() {
        this.database = FirebaseDatabase.getInstance(this.url);
        this.storage = FirebaseStorage.getInstance();
    }

    // Agafem les dades dels diferents àmbits i les anem modificant si hi ha canvis
    public void ambitsList(AmbitRecyclerViewAdapter mAdapter, List<Ambit> ambits) {

        DatabaseReference databaseReference = this.database.getReference(ambitPath);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Ambit ambit = dataSnapshot.getValue(Ambit.class);
                    int i = ambits.indexOf(ambit);
                    if (i >= 0) {
                        ambits.get(i).updateData(ambit);
                        Log.e("AmbitList ambitChange", dataSnapshot.getValue(Ambit.class).getAmbitID());
                    } else {
                        ambits.add(ambit);
                        Log.e("AmbitList ambitAdd", dataSnapshot.getValue(Ambit.class).getAmbitID());
                    }
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("firebase onCancelled", "Error getting data", error.toException());
            }
        });
    }

    // Agafem les dades de l'usuari i les modifiquem si hi ha canvis
    public void listenUserData(String userID, ListenerUserFirebaseAdapter listener) {

        DatabaseReference databaseReference = FirebaseDatabase
                .getInstance("https://assemblea-c8e01-default-rtdb.europe-west1.firebasedatabase.app/")
                .getReference(userPath)
                .child(userID);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);

                listener.fillLayout(user);
                Log.e("Firebase onDataChange", "Dades usuari actualitzades");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("firebase onCancelled", "Error getting data", error.toException());
            }
        });
    }

    // Creem les dades de l'usuari
    public void createUserData(User user) {
        this.database.getReference(userPath).child(user.getUserID()).setValue(user);
    }

    // Comprovem l'existència d'un usuari
    public boolean userExist(String userId) {
        return (null != getUser(userId));
    }

    // Agafem les dades de l'usuari
    public User getUser(String userId) {
        DatabaseReference databaseReference = this.database.getReference(userPath)
                .child(userId);
        Task<DataSnapshot> task = databaseReference.get();
        while (!task.isComplete()) ;
        return task.getResult().getValue(User.class);
    }

    // Canviem l'estat de l'usuari
    public void changeUserState(String userID, boolean newState) {

        DatabaseReference databaseReference = this.database
                .getReference(userPath)
                .child(userID);

        databaseReference.child("state").setValue(newState);

    }

    // Agafem tot el nom de l'usuari (nom+cognom)
    public String getAllUserName(String userID) {
        return getUser(userID).getAllName();
    }

    // Afegim un nou àmbit a votar a la BD
    public void addAmbit(Ambit ambit) {
        this.database.getReference(ambitPath).child(ambit.getAmbitID()).setValue(ambit);
    }

    // Modifiquem un àmbit a votar a la BD
    public void editAmbit(Ambit ambit) {

        DatabaseReference databaseReference = this.database
                .getReference(ambitPath)
                .child(ambit.getAmbitID());

        databaseReference.setValue(ambit);

    }

    // Votem
    public void doVote(String ambitID, String userId, String vote) {
        this.database.getReference(votePath).child(ambitID).child(userId).setValue(vote);
    }

    // Afegim un vot a l'usuari
    public void addVoteToUser(String userID, String ambitID, String vote) {
        this.database.getReference(userPath).child(userID).child("votes").child(ambitID).setValue(vote);
    }

    // Fem el recompte de vots
    public Vote doRecount(String ambitID) {

        DatabaseReference databaseReference = this.database
                .getReference(votePath)
                .child(ambitID);

        Task<DataSnapshot> task = databaseReference.get();
        while (!task.isComplete()) ;

        HashMap<String, String> votes = (HashMap<String, String>) task.getResult().getValue();

        int aFavor = 0;
        int enContra = 0;
        int abstencio = 0;
        int blank = 0;
        for (Map.Entry<String, String> set : votes.entrySet()) {

            switch (set.getValue()) {
                case ("sí"):
                    aFavor++;
                    break;
                case ("no"):
                    enContra++;
                    break;
                case ("abstencio"):
                    abstencio++;
                    break;
                case ("blank"):
                    blank++;
                    break;
                default:
                    break;
            }
        }

        Vote vote = new Vote(aFavor, enContra, abstencio, blank);

        this.database.getReference(ambitPath).child(ambitID).child("votes").setValue(vote);

        return vote;
    }

    // Xifrem la taula de vots d'un àmbit
    public void codeFirebaseVotesOfOneAmbit(String ambitID) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        // Agafo tots els vots efectuats en un àmbit
        DatabaseReference databaseReference = this.database.getReference(votePath)
                .child(ambitID);
        Task<DataSnapshot> task = databaseReference.get();
        while (!task.isComplete()) ;
        HashMap<String, String> ambitVotes = (HashMap<String, String>) task.getResult().getValue();

        databaseReference.removeValue();

        // Xifro el conjunt de dades

        String data = ambitVotes.toString();

        // Xifrar les dades data

        KeyGenerator kgenerator = KeyGenerator.getInstance("AES");
        SecureRandom random = new SecureRandom();
        kgenerator.init(256, random);
        Key aeskey = kgenerator.generateKey();
        byte[] raw = aeskey.getEncoded();
        // Create key and cipher
        Key aesKey = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        // encrypt the text
        cipher.init(Cipher.ENCRYPT_MODE, aesKey);
        byte[] encrypted = cipher.doFinal(data.getBytes());
        System.err.println(new String(encrypted));

        // Desxifrar les dades the text
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        String decrypted = new String(cipher.doFinal(encrypted));
        System.err.println(decrypted);

        // Torno a desar el conjunt de dades, aquest cop xifrat
        databaseReference.child("xifrat").setValue(new String(encrypted));

    }

    public void uploadDocument(String ambitID, Intent data) {

        final StorageReference documentReference = this.storage.getReference(documentPath + "/" + ambitID + ".pdf");
        documentReference.putFile(data.getData());

    }

    public Uri getDocument(String ambitID) {

        final StorageReference documentReference = this.storage.getReference(documentPath + "/" + ambitID + ".pdf");
        Task<Uri> uriTask = documentReference.getDownloadUrl();

        while (!uriTask.isComplete()) ;

        return uriTask.getResult();
    }


    public Uri getUserImage(String userID) {
        final StorageReference documentReference = this.storage.getReference(imagesPath + "/" + userID);
        Task<Uri> uriTask = documentReference.getDownloadUrl();
        while (!uriTask.isComplete()) ;

        if(uriTask.getException() instanceof StorageException) {
            return null;
        }
        return uriTask.getResult();

    }

    public void saveUserImage(String userID, Intent data) {
        final StorageReference documentReference = this.storage.getReference(imagesPath + "/" + userID);
        documentReference.putFile(data.getData());
    }
}
