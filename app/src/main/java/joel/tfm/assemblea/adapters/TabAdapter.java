package joel.tfm.assemblea.adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import joel.tfm.assemblea.fragments.DecisionsFragment;
import joel.tfm.assemblea.fragments.UserFragment;

public class TabAdapter extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public TabAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new UserFragment();
            case 1:
                return new DecisionsFragment();
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}
