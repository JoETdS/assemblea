package joel.tfm.assemblea.adapters;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import joel.tfm.assemblea.R;
import joel.tfm.assemblea.classes.Ambit;
import joel.tfm.assemblea.classes.Vote;
import joel.tfm.assemblea.fragments.DecisionsFragment.OnListFragmentInteractionListener;
import joel.tfm.assemblea.interfaces.ListenerDecisionsRecyclerView;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Ambit}.
 * TODO: Replace the implementation with code for your data type.
 */
public class AmbitRecyclerViewAdapter extends RecyclerView.Adapter<AmbitRecyclerViewAdapter.ViewHolder> {

    private final List<Ambit> ambits;
    private final OnListFragmentInteractionListener mListener;
    private final ListenerDecisionsRecyclerView listenerDecisionsRecyclerView;

    private PieChart pieChart;
    private PieData pieData;
    private PieDataSet pieDataSet;
    private List<PieEntry> pieEntries;
    private List<PieEntry> PieEntryLabels;

    private SharedPreferences sharedPreferences;

    public AmbitRecyclerViewAdapter(List<Ambit> items, OnListFragmentInteractionListener listener,
                                    ListenerDecisionsRecyclerView listenerDecisionsRecyclerView) {
        ambits = items;
        mListener = listener;
        this.listenerDecisionsRecyclerView = listenerDecisionsRecyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.decisions_card, parent, false);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(parent.getContext());
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Ambit ambit = holder.mAmbit = ambits.get(position);
        holder.mNameView.setText(ambit.getName());
        holder.mDescriptionView.setText(ambit.getDescription());
        boolean expanded = ambit.isExpanded();
        holder.mExpandableCard.setVisibility(expanded ? View.VISIBLE : View.GONE);
        holder.mVoteButton.setIcon(ContextCompat.getDrawable(holder.mVoteButton.getContext(), expanded ?
                R.drawable.ic_baseline_keyboard_arrow_up_24 :
                R.drawable.ic_baseline_keyboard_arrow_down_24));

        dadesPieChart(holder.mPieChart, ambit.getName());

        getEntries(ambit.getVotes());

        selectExpandableView(ambit.getState(), holder.mCardStates);

        holder.mVoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, ambit.getName(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                ambit.setExpanded(!ambit.isExpanded());
                notifyItemChanged(position);
            }
        });

        if (isTecnic() && !ambit.getState().equals("Tancada")) {
            holder.mEditButton.setVisibility(View.VISIBLE);
        }

        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listenerDecisionsRecyclerView.editAmbitScreen(ambit);
                notifyItemChanged(position);
            }
        });

        holder.mButtonVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String vote = "";
                switch (holder.radioGroupVote.getCheckedRadioButtonId()) {
                    case R.id.radioButton_yes:
                        vote = "sí";
                        break;
                    case R.id.radioButton_no:
                        vote = "no";
                        break;
                    case R.id.radioButton_abs:
                        vote = "abstencio";
                        break;
                    case R.id.radioButton_blank:
                        vote = "blank";
                        break;
                    default:
                        vote = "res";
                        break;
                }
                Snackbar.make(v, "Ha votat: " + vote, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                listenerDecisionsRecyclerView.doVote(ambit.getAmbitID(), vote);
            }
        });

        holder.mDownloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listenerDecisionsRecyclerView.getAmbitFile(ambit.getAmbitID());
            }
        });

    }

    private boolean isTecnic() {

        String type = sharedPreferences.getString("type", "Membre");
        return type.equals("Tècnic");
    }


    private void selectExpandableView(String state, ConstraintLayout cards[]) {

        switch (state) {
            case "Prèvia":
                cards[0].setVisibility(View.VISIBLE);
                cards[1].setVisibility(View.GONE);
                cards[2].setVisibility(View.GONE);
                break;
            case "Oberta":
                if(listenerDecisionsRecyclerView.getCurrentUser() != null &&
                        (listenerDecisionsRecyclerView.getCurrentUser().getState()) &&
                        !(listenerDecisionsRecyclerView.getCurrentUser().getType().equals("Membre"))){
                    cards[0].setVisibility(View.GONE);
                    cards[1].setVisibility(View.VISIBLE);
                    cards[2].setVisibility(View.GONE);
                }else{
                cards[0].setVisibility(View.VISIBLE);
                cards[1].setVisibility(View.GONE);
                cards[2].setVisibility(View.GONE);
                }
                break;
            case "Tancada":
                cards[0].setVisibility(View.GONE);
                cards[1].setVisibility(View.GONE);
                cards[2].setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }

    }


    private void dadesPieChart(PieChart pieChart, String nomVotacio) {
        pieDataSet = new PieDataSet(pieEntries, "");
        pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        pieDataSet.setValueTextColor(Color.BLACK);
        pieChart.setEntryLabelColor(Color.BLACK);
        pieDataSet.setValueTextSize(16f);
        pieDataSet.setDrawValues(true);
        pieChart.setCenterText("Vots a " + nomVotacio);
        pieChart.getDescription().setEnabled(false);

        pieChart.animateY(2000, Easing.EaseInCubic);

    }

    private void getEntries(Vote votes) {
        pieEntries = new ArrayList<>();
        if (null != votes) {
            pieEntries.add(new PieEntry(votes.getaFavor(), "Sí"));
            pieEntries.add(new PieEntry(votes.getEnContra(), "No"));
            pieEntries.add(new PieEntry(votes.getAbstencio(), "Abstenció"));
            pieEntries.add(new PieEntry(votes.getBlank(), "Nul"));
        }
    }

    @Override
    public int getItemCount() {
        return ambits.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final TextView mDescriptionView;
        public final MaterialCardView mCard;
        public final ConstraintLayout[] mCardStates;
        public final MaterialButton mVoteButton;
        public final MaterialButton mEditButton;
        public final MaterialButton mDownloadButton;
        public final MaterialButton mButtonVote;
        public final MaterialRadioButton rb_yes, rb_no, rb_abs, rb_nul;
        public final RadioGroup radioGroupVote;
        public final LinearLayout mExpandableCard;
        public final PieChart mPieChart;
        public Ambit mAmbit;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCard = (MaterialCardView) view.findViewById(R.id.card_ambit);
            mNameView = (TextView) view.findViewById(R.id.TVambit_name);
            mDescriptionView = (TextView) view.findViewById(R.id.TVambit_description);
            mVoteButton = (MaterialButton) view.findViewById(R.id.Bambit_votacio);
            mEditButton = (MaterialButton) view.findViewById(R.id.Bambit_editar);
            mDownloadButton = (MaterialButton) view.findViewById(R.id.Bambit_descarregar);
            mExpandableCard = (LinearLayout) view.findViewById(R.id.expandable_card);

            //Radio buttons
            rb_yes = (MaterialRadioButton) view.findViewById(R.id.radioButton_yes);
            rb_no = (MaterialRadioButton) view.findViewById(R.id.radioButton_no);
            rb_abs = (MaterialRadioButton) view.findViewById(R.id.radioButton_abs);
            rb_nul = (MaterialRadioButton) view.findViewById(R.id.radioButton_blank);
            radioGroupVote = (RadioGroup) view.findViewById(R.id.radio_group_vote);
            mButtonVote = (MaterialButton) view.findViewById(R.id.button_vote);

            // Opcions en funció de l'estat de l'àmbit
            mCardStates = new ConstraintLayout[3];
            mCardStates[0] = (ConstraintLayout) view.findViewById(R.id.expandable_card_previous);
            mCardStates[1] = (ConstraintLayout) view.findViewById(R.id.expandable_card_assembly);
            mCardStates[2] = (ConstraintLayout) view.findViewById(R.id.expandable_card_post);

            mPieChart = (PieChart) mCardStates[2].findViewById(R.id.pieChart);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
