package joel.tfm.assemblea.classes;

import java.util.HashMap;
import java.util.UUID;

public class User {

    private String userID;
    private String name;
    private String surname;
    private String group;
    private String type;
    // True -> in, False -> out
    private boolean state;

    private HashMap<String, String> votes;
    private HashMap<String, String> passwords;

    public User() {
    }

    public User(String name, String surname, String group, String type) {
        this.userID = UUID.randomUUID().toString();
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.type = type;
        this.state = false;
        this.votes = new HashMap<>();
    }

    public User(String userID, String name, String surname, String group, String type) {
        this.userID = userID;
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.type = type;
        this.state = false;
        this.votes = new HashMap<>();
    }

    public HashMap<String, String> getVotes() {
        return votes;
    }

    public void setVotes(HashMap<String, String> votes) {
        this.votes = votes;
    }

    public String getUserID() {
        return userID;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getGroup() {
        return group;
    }

    public String getType() {
        return type;
    }

    public boolean getState() {
        return state;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public String getQRInformation() {
        return getUserID() + " " + getState();
    }

    public String getAllName() {
        return getName() + " " + getSurname();
    }

    public void doVote(String ambitID, String vote) {
        if (this.votes == null) {
            this.votes = new HashMap<>();
            this.votes.put(ambitID, vote);
        } else {
            this.votes.put(ambitID, vote);
        }
    }

    public String getVote(String ambitID) {
        if (this.votes != null && this.votes.containsKey(ambitID)) {
            return this.votes.get(ambitID);
        } else {
            return null;
        }
    }

    public void encriptVote(String ambitID, int hashedPassword) {
        String vote = this.votes.get(ambitID);
        this.votes.put(ambitID, String.valueOf(hashedPassword));
    }
}
