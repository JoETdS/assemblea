package joel.tfm.assemblea.classes;

public class Vote {

    private int aFavor;
    private int enContra;
    private int abstencio;
    private int blank;

    public Vote() {
    }

    public Vote(int aFavor, int enContra, int abstencio, int blank) {
        this.aFavor = aFavor;
        this.enContra = enContra;
        this.abstencio = abstencio;
        this.blank = blank;
    }

    public void setaFavor(int aFavor) {
        this.aFavor = aFavor;
    }

    public void setEnContra(int enContra) {
        this.enContra = enContra;
    }

    public void setAbstencio(int abstencio) {
        this.abstencio = abstencio;
    }

    public void setBlank(int blank) {
        this.blank = blank;
    }

    public int getaFavor() {
        return aFavor;
    }

    public int getEnContra() {
        return enContra;
    }

    public int getAbstencio() {
        return abstencio;
    }

    public int getBlank() {
        return blank;
    }
}
