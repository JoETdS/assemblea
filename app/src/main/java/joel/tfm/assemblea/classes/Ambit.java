package joel.tfm.assemblea.classes;

import androidx.annotation.Nullable;

import java.util.UUID;

public class Ambit {

    private String ambitID;
    private String name;
    private String description;
    private Vote votes;
    private String document;
    private String state;
    private boolean expanded;

    public Ambit() {
        this.expanded = false;
    }

    public Ambit(String name, String description, Vote votes, String document, String state) {
        this.ambitID = UUID.randomUUID().toString();
        this.name = name;
        this.description = description;
        this.votes = votes;
        this.document = document;
        this.state = state;
        this.expanded = false;
    }

    public Ambit(String ambitID, String name, String description, String document, String state, Vote votes) {
        this.ambitID = ambitID;
        this.name = name;
        this.description = description;
        this.votes = votes;
        this.document = document;
        this.state = state;
        this.expanded = false;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        super.equals(obj);
        if (obj instanceof Ambit) {
            return this.ambitID.equals(((Ambit) obj).ambitID);
        } else {
            return false;
        }
    }

    public String getAmbitID() {
        return ambitID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Vote getVotes() {
        return votes;
    }

    public void setVotes(Vote votes) {
        this.votes = votes;
    }

    public String getDocument() {
        return document;
    }

    public String getState() {
        return state;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }


    public void updateData(Ambit ambit) {
        this.name = ambit.getName();
        this.description = ambit.getDescription();
        this.votes = ambit.getVotes();
        this.document = ambit.getDocument();
        this.state = ambit.getState();
    }
}