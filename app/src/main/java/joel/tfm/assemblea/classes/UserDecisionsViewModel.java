package joel.tfm.assemblea.classes;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class UserDecisionsViewModel extends ViewModel {

    private MutableLiveData<User> currentUser = new MutableLiveData<>();
    public void setCurrentUser(User user) {
        currentUser.setValue(user);
    }
    public LiveData<User> getCurrentUser() {
        return currentUser;
    }
}
