package joel.tfm.assemblea.fragments;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import joel.tfm.assemblea.R;
import joel.tfm.assemblea.adapters.AmbitRecyclerViewAdapter;
import joel.tfm.assemblea.adapters.FirebaseDatabaseAdapter;
import joel.tfm.assemblea.classes.Ambit;
import joel.tfm.assemblea.classes.User;
import joel.tfm.assemblea.classes.UserDecisionsViewModel;
import joel.tfm.assemblea.classes.Vote;
import joel.tfm.assemblea.interfaces.ListenerDecisionsRecyclerView;

import static android.app.Activity.RESULT_OK;


public class DecisionsFragment extends Fragment implements ListenerDecisionsRecyclerView {

    // RecyclerView
    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private List<Ambit> ambits = new ArrayList<Ambit>();
    private AmbitRecyclerViewAdapter ambitRecyclerViewAdapter;

    //Database
    // Adaptaer DB
    private FirebaseDatabaseAdapter FBDBAdapter;
    // SharedPreferences
    private SharedPreferences sharedPreferences;

    // Button add Decision
    private FloatingActionButton btn_fab_add;

    // Auxiliar for ambitID
    private String isNewAmbit;
    private int auxPos;

    // ViewModel between fragments
    private UserDecisionsViewModel userDecisionsViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }


    // Incloiem els Ambits inicials i deixem la connexió creada per futures modificacions
    private void addAndListenAmbits() {
        try {
            connectDB();
            addInitialAmbits();
        } catch (Exception e) {

        }

    }

    private void connectDB() {
        this.FBDBAdapter = new FirebaseDatabaseAdapter();
    }

    private void addInitialAmbits() {
        this.FBDBAdapter.ambitsList(ambitRecyclerViewAdapter, ambits);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        View view = inflater.inflate(R.layout.fragment_decisions, container, false);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        // Set the adapter
        if (view.findViewById(R.id.list_decisions) instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list_decisions);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            this.ambitRecyclerViewAdapter = new AmbitRecyclerViewAdapter(this.ambits, mListener, this);
            recyclerView.setAdapter(this.ambitRecyclerViewAdapter);
        }

        // Add ambits to list
        addAndListenAmbits();

        loadLayoutElements(view);

        this.userDecisionsViewModel = new ViewModelProvider(requireActivity()).get(UserDecisionsViewModel.class);

        // Inflate the layout for this fragment
        return view;
    }

    // Carregar elements per pantalla
    private void loadLayoutElements(View view) {

        String[] arrayStates = new String[]{"Prèvia", "Oberta", "Tancada"};
        AutoCompleteTextView autocomplete = (AutoCompleteTextView) view.findViewById(R.id.TIDambit_estat_edit);
        ArrayAdapter<String> autoAdapter;
        autoAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                arrayStates);
        autocomplete.setAdapter(autoAdapter);

        btn_fab_add = getActivity().findViewById(R.id.fab_add);
        btn_fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAmbitScreen();
            }
        });

        MaterialButton buton_update_ambit = view.findViewById(R.id.button_ambit_update);
        buton_update_ambit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText name = view.findViewById(R.id.ETambit_name_edit);
                TextInputEditText description = view.findViewById(R.id.ETambit_description_edit);
                MaterialAutoCompleteTextView state = view.findViewById(R.id.TIDambit_estat_edit);
                if (isNewAmbit == null) {
                    Vote vote = new Vote(0, 0, 0, 0);
                    Ambit ambit = new Ambit(name.getText().toString(), description.getText().toString(), vote, "", state.getText().toString());
                    addAmbitDataToFireBase(ambit);
                } else {
                    Ambit ambit = new Ambit(isNewAmbit, name.getText().toString(), description.getText().toString(), "", state.getText().toString(), ambits.get(auxPos).getVotes());
                    editAmbitDataToFireBase(ambit);
                    if(state.getText().toString().equals("Tancada")){
                        try {
                            closeAmbit(ambit);
                        } catch (IllegalBlockSizeException e) {
                            e.printStackTrace();
                        } catch (InvalidKeyException e) {
                            e.printStackTrace();
                        } catch (BadPaddingException e) {
                            e.printStackTrace();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (NoSuchPaddingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                btn_fab_add.setVisibility(View.VISIBLE);
                view.findViewById(R.id.add_decision).setVisibility(View.GONE);
                ambitRecyclerViewAdapter.notifyDataSetChanged();
            }
        });

    }

    // Si un àmbit passa d'estar-se votant a tancat
    private void closeAmbit(Ambit ambit) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        ambit.setVotes(this.FBDBAdapter.doRecount(ambit.getAmbitID()));
        // Xifratge de la taula (userID-vote) de vots d'un àmbit
        this.FBDBAdapter.codeFirebaseVotesOfOneAmbit(ambit.getAmbitID());
    }

    // Afegir un nou ambit a la Base da dades
    private void addAmbitDataToFireBase(Ambit ambit) {
        this.ambits.add(ambit);
        ambitRecyclerViewAdapter.notifyDataSetChanged();
        this.FBDBAdapter.addAmbit(ambit);
    }

    // Modificar la informació d'un àmbit
    private void editAmbitDataToFireBase(Ambit ambit) {
        int i = this.ambits.indexOf(ambit);
        this.ambits.get(i).updateData(ambit);
        this.FBDBAdapter.editAmbit(ambit);
        ambitRecyclerViewAdapter.notifyDataSetChanged();
    }

    // Mostrar la pantalla per afegir un Àmbit
    private void addAmbitScreen() {
        isNewAmbit = null;
        getView().findViewById(R.id.add_decision).setVisibility(View.VISIBLE);
        TextInputEditText name = getView().findViewById(R.id.ETambit_name_edit);
        name.setText("");
        TextInputEditText description = getView().findViewById(R.id.ETambit_description_edit);
        description.setText("");
        btn_fab_add.setVisibility(View.GONE);
        Snackbar.make(getView(), "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    // Mostrar la pantalla per modificar un àmbit, mostrant les dades que ja té
    public void editAmbitScreen(Ambit ambit) {
        isNewAmbit = ambit.getAmbitID();
        auxPos = ambits.indexOf(ambit);
        getView().findViewById(R.id.add_decision).setVisibility(View.VISIBLE);
        TextInputEditText name = getView().findViewById(R.id.ETambit_name_edit);
        name.setText(ambit.getName());
        TextInputEditText description = getView().findViewById(R.id.ETambit_description_edit);
        MaterialButton button_description = getView().findViewById(R.id.Bambit_pujar_edit);
        button_description.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  chooseDocument();
              }
          });
        btn_fab_add.setVisibility(View.GONE);
        Snackbar.make(getView(), "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void chooseDocument() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // We will be redirected to choose pdf
        galleryIntent.setType("application/pdf");
        startActivityForResult(galleryIntent, 1);
    }

    ProgressDialog dialog;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Uploading");
            this.FBDBAdapter.uploadDocument(isNewAmbit, data);
        }


    }


    // Realitzar el vot
    @Override
    public void doVote(String ambitID, String vote) {

        if (!hasUserVoted(ambitID)) {
            getCurrentUser().doVote(ambitID, vote);
            String userID = getCurrentUser().getUserID();

            this.FBDBAdapter.addVoteToUser(userID, ambitID, vote);

            this.FBDBAdapter.doVote(ambitID, userID, vote);
        }
    }

    // [WIP] Encriptar el vot
    private void encriptVote(String ambitID){
        int hashedPassword = sharedPreferences.getInt("password", 0);
        getCurrentUser().encriptVote(ambitID, hashedPassword);
    }

    // Comprovem si l'usuari a votat, en cas afirmatiu no pot votar
    private boolean hasUserVoted(String ambitID) {
        return getCurrentUser().getVote(ambitID) != null;
    }

    // Agafem les dades de l'usuari actual
    public User getCurrentUser() {
        return userDecisionsViewModel.getCurrentUser().getValue();
    }

    @Override
    public void getAmbitFile(String ambitID) {

        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setDataAndType(this.FBDBAdapter.getDocument(ambitID), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Intent newIntent = Intent.createChooser(intent, "Open File");
        try {
            startActivity(newIntent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Ambit item);
    }

}