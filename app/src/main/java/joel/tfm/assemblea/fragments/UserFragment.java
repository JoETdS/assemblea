package joel.tfm.assemblea.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import joel.tfm.assemblea.R;
import joel.tfm.assemblea.adapters.FirebaseDatabaseAdapter;
import joel.tfm.assemblea.classes.User;
import joel.tfm.assemblea.classes.UserDecisionsViewModel;
import joel.tfm.assemblea.interfaces.ListenerUserFirebaseAdapter;
import joel.tfm.assemblea.ui.login.LoginActivity;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class UserFragment extends Fragment implements ListenerUserFirebaseAdapter {

    //Database
    // User auth
    private FirebaseUser userFB;
    // Data
    private FirebaseDatabaseAdapter FBDBadapter;

    // User
    private User user;

    // Images
    private ShapeableImageView Iuser_image;
    private ShapeableImageView Iuser_QR;

    // User data TextViews
    private MaterialTextView TVuser_name;
    private MaterialTextView TVuser_surname;
    private MaterialTextView TVuser_agrupament;
    private MaterialTextView TVuser_type;
    private MaterialTextView TVuser_state;

    // Buttons
    private MaterialButton Buser_disconnect;
    private MaterialButton Buser_scanQR;
    private FloatingActionButton fab_add;

    // ViewModel between fragments
    private UserDecisionsViewModel userDecisionsViewModel;

    private SharedPreferences sharedPreferences;

    private int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1000;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user, container, false);

        // Start FireBaseConnection
        this.FBDBadapter = new FirebaseDatabaseAdapter();

        // Instance of SharedPreferences
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        this.userDecisionsViewModel = new ViewModelProvider(requireActivity()).get(UserDecisionsViewModel.class);

        // Listen all elements from layout
        loadLayoutElements(view);

        // Load data user from firebase
        loadUserData(view);

        return view;
    }

    private void loadLayoutElements(View view) {

        // Images
        this.Iuser_image = view.findViewById(R.id.Iuser_image);
        this.Iuser_QR = view.findViewById(R.id.Iuser_QR);

        // User data TextViews
        this.TVuser_name = view.findViewById(R.id.TVuser_name);
        this.TVuser_surname = view.findViewById(R.id.TVuser_surname);
        this.TVuser_agrupament = view.findViewById(R.id.TVuser_agrupament);
        this.TVuser_type = view.findViewById(R.id.TVuser_type);
        this.TVuser_state = view.findViewById(R.id.TVuser_state);

        // Button
        this.Buser_disconnect = view.findViewById(R.id.button_disconnect);
        this.Buser_disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });

        this.Buser_scanQR = view.findViewById(R.id.button_scan);

        this.Buser_scanQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes

                    startActivityForResult(intent, 0);

                } catch (Exception e) {

                    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);

                }
            }
        });

        this.fab_add = getActivity().findViewById(R.id.fab_add);

        this.Iuser_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkStoragePermision()){
                    cangeUserImage();
                }
            }
        });

    }

    private void cangeUserImage() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);

    }

    // Un cop ens retorna el valor del QR, el tractem
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {

            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                String[] dataQR = contents.split(" ");
                String goodQR = changeUserState(dataQR[0], Boolean.parseBoolean(dataQR[1]));

                Snackbar.make(getView(), goodQR, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            if (resultCode == RESULT_CANCELED) {
                Snackbar.make(getView(), getText(R.string.incorrectQR).toString(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }else if(requestCode == 1){
            if (data != null){
                changeUserImage(data);
            }
        }
    }

    private void changeUserImage(Intent data) {
        this.FBDBadapter.saveUserImage(this.user.getUserID(), data);
        Glide.with(this /* context */)
                .load(data)
                .into(this.Iuser_image);
    }

    private boolean checkStoragePermision() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }else{
            return true;
        }
        return false;
    }

    @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

            if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // Permission Denied
                    Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    // Modifiquem l'estat de l'usuari dins-fora la sala de votació
    public String changeUserState(String userID, Boolean state) {
        String blank = " ";
        if (state) {
            String correctQRend = getText(R.string.correctQRExit).toString();
            // Si està dins (true), surt a l'exterior (false)
            String name = changeStateFirebase(userID, false);
            return name + correctQRend;
        } else {
            String correctQRend = getText(R.string.correctQREntry).toString();
            // Si està fora (false), surt a l'exterior (true)
            String name = changeStateFirebase(userID, true);
            return name + correctQRend;
        }
    }

    // Modiquem l'estat de l'usuari a la BD
    private String changeStateFirebase(String userID, boolean newState) {

        this.FBDBadapter.changeUserState(userID, newState);

        return this.FBDBadapter.getAllUserName(userID);

    }

    // L'usuari es desconnecta
    private void disconnect() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("password", 0);
        editor.apply();
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.signOut();
    }

    // Carreguem dades de l'usuari
    private void loadUserData(View view) {
        this.userFB = FirebaseAuth.getInstance().getCurrentUser();
        if (this.userFB != null) {
            this.user = getDummyUser();
            getUserData(this.userFB.getUid());
        }
    }

    // Mostrem per pantalla les dades de l'usuari
    public void fillLayout(User user) {
        this.user = user;
        userDecisionsViewModel.setCurrentUser(this.user);
        userDataText();
        imageUser();
        getQR();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("type", user.getType());
        editor.putString("userID", user.getUserID());
        editor.apply();

        if (isTecnic()) {
            this.Buser_scanQR.setVisibility(View.VISIBLE);
            fab_add.setVisibility(View.VISIBLE);
        }
    }

    // Comprovem si l'usuari és del tipus tècnic
    private boolean isTecnic() {
        return this.user.getType().equals(getText(R.string.tecnic).toString());
    }

    // Carreguem la imatge de l'usuari
    private void imageUser() {

        Uri imageUri = this.FBDBadapter.getUserImage(this.user.getUserID());

        if (imageUri != null){
            Glide.with(this /* context */)
                    .load(imageUri)
                    .into(this.Iuser_image);
        }
    }

    // Carreguem el QR de l'usuari a la seva informació
    private void getQR() {
        String data = user.getQRInformation();
        QRGEncoder qrgEncoder = new QRGEncoder(data, null, QRGContents.Type.TEXT, 320);
        qrgEncoder.setColorBlack(Color.BLACK);
        qrgEncoder.setColorWhite(Color.WHITE);
        // Getting QR-Code as Bitmap
        Bitmap bitmap = qrgEncoder.getBitmap();
        // Setting Bitmap to ImageView
        this.Iuser_QR.setImageBitmap(bitmap);

    }

    // Modifiquem les dades per pantalla de l'usuari per mostar-ne el seu valor
    private void userDataText() {

        this.TVuser_name.setText(this.user.getName());
        this.TVuser_surname.setText(this.user.getSurname());
        this.TVuser_agrupament.setText(this.user.getGroup());
        this.TVuser_type.setText(this.user.getType());
        this.TVuser_state.setText(this.user.getState() ? "dins" : "fora");

    }

    // Agafem les dades de l'usuari de la BD
    private void getUserData(String userID) {

        this.FBDBadapter.listenUserData(userID, this);

    }

    // Informació a mostrar mentre les dades de l'usuari no s'han aconseguit sincronitzar
    private User getDummyUser() {
        return new User(getText(R.string.user_name).toString(),
                getText(R.string.user_surname).toString(),
                getText(R.string.user_agrupament).toString(),
                getText(R.string.user_type).toString());
    }

}