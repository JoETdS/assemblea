package joel.tfm.assemblea.interfaces;

import java.io.File;

import joel.tfm.assemblea.classes.Ambit;
import joel.tfm.assemblea.classes.User;

public interface ListenerDecisionsRecyclerView {
    void editAmbitScreen(Ambit ambit);
    void doVote(String ambitID, String vote);
    User getCurrentUser();
    void getAmbitFile(String ambitId);

}
