# Appssemblea, Aplicació per Assemblees

Aplicació per a la realització d'assemblees presencials mitjançant un dispositiu mòbil per agilitzar el procés de recompte de vots. 

## Manual de l'aplicació

[Manual](https://joetds.gitlab.io/assemblea)

## Informació general

Aquesta aplicació té l'objectiu d'agilitzar el procés de votació i recompte de vots d'una assemblea a través de l'utilització del dispositiu mòbil.

Entre les seves característiques principals podem trobar:

* Crear espais de votació
* Modificar espais de votació
* Realitzar la votació
* Consultar els resultats de les votació
* Comprovar l'accés a l'espai de l'assemblea
* Consultar la documentació de l'assemblea

## Imatges de aplicació

![Perfil usuari](docs/perfils/imatges/membre/perfil_membre.png)


![Consulta resultats](docs/perfils/imatges/compromissari/resultats_compromissari.png)


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/es/"><img alt="Llicència de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/es/88x31.png" /></a><br />Aquesta obra està subjecta a una llicència de <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/es/">Reconeixement-NoComercial-CompartirIgual 3.0 Espanya de Creative Commons</a>
